package pl.davidduke.ismessage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsMessageApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsMessageApplication.class, args);
	}

}
